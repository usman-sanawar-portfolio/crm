﻿using Core.Common;
using Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.UserData
{
    public class User : AuditableEntity
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public ICollection<Account> Accounts { get; set; } = new List<Account>();
        public ICollection<User> Users { get; set; } = new List<User>();
    }
}
