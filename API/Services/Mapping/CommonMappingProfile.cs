﻿using AutoMapper;
using Core.Domain.MasterData;
using Core.Dtos.MasterData;

namespace API.Services.Mapping
{
    public class CommonMappingProfile : Profile
    {
        public CommonMappingProfile()
        {
            CreateMap<Currency, CurrencyDto>();
            CreateMap<CurrencyDto, Currency>();

            CreateMap<Industry, IndustryDto>();
            CreateMap<IndustryDto, Industry>();

            CreateMap<AccountType, AccountTypeDto>();
            CreateMap<AccountTypeDto, AccountType>();

            CreateMap<EmailAddress, EmailAddressDto>()
                .ForMember(x => x.ContactName, x => x.MapFrom(c => c.Contact.FirstName + " " + c.Contact.LastName))
                .ForMember(x => x.LeadName, x => x.MapFrom(c => c.Lead.FirstName + " " + c.Lead.LastName))
                .ForMember(x => x.AccountName, x => x.MapFrom(c => c.Account.Name));
            CreateMap<EmailAddressDto, EmailAddress>();

            CreateMap<LeadSource, LeadSourceDto>();
            CreateMap<LeadSourceDto, LeadSource>();

            CreateMap<PrefixTitle, PrefixTitleDto>();
            CreateMap<PrefixTitleDto, PrefixTitle>();

            CreateMap<Status, StatusDto>();
            CreateMap<StatusDto, Status>();

            CreateMap<Account, AccountDto>()
                .ForMember(c => c.AssignedToUserId, x => x.MapFrom(c => c.User.UserId))
                .ForMember(c => c.AssignedToUsername, x => x.MapFrom(c => c.User.Username))
                .ForMember(c => c.AssignedToFullName, x => x.MapFrom(c => c.User.FullName))
                .ForMember(c => c.AccountTypeName, x => x.MapFrom(c => c.AccountType.AccountTypeName))
                .ForMember(c => c.IndustryName, x => x.MapFrom(c => c.Industry.IndustryName))
                .ForMember(x => x.CreatedByName, x => x.MapFrom(x => x.CreatedBy.FullName));
            CreateMap<AccountDto, Account>()
                .ForMember(x => x.UserId, x => x.MapFrom(c => c.AssignedToUserId));

            CreateMap<Contact, ContactDto>()
                .ForMember(x => x.AssignedToUserId, x => x.MapFrom(c => c.AssignedToUser.UserId))
                  .ForMember(c => c.AssignedToUsername, x => x.MapFrom(c => c.AssignedToUser.Username))
                .ForMember(c => c.AssignedToFullName, x => x.MapFrom(c => c.AssignedToUser.FullName))
                .ForMember(c => c.AccountName, x => x.MapFrom(c => c.Account.Name))
                .ForMember(x => x.LeadSourceDescription, x => x.MapFrom(c => c.LeadSource.LeadSourceTitle))
                .ForMember(x => x.PrefixTitleName, x => x.MapFrom(x => x.PrefixTitle.Title))
                .ForMember(x => x.CreatedByName, x => x.MapFrom(x => x.CreatedBy.FullName));
            CreateMap<ContactDto, Contact>();

            CreateMap<Lead, LeadDto>()
                .ForMember(x => x.AssignedToUserId, x => x.MapFrom(c => c.AssignedToUser.UserId))
                  .ForMember(c => c.AssignedToUsername, x => x.MapFrom(c => c.AssignedToUser.Username))
                .ForMember(c => c.AssignedToFullName, x => x.MapFrom(c => c.AssignedToUser.FullName))
                .ForMember(x => x.PrefixTitleName, x => x.MapFrom(x => x.PrefixTitle.Title))
                .ForMember(x => x.StatusData, x => x.MapFrom(x => x.Status.Description))
                .ForMember(x => x.LeadSourceData, x => x.MapFrom(x => x.LeadSource.Description))
                .ForMember(x => x.CreatedByName, x => x.MapFrom(x => x.CreatedBy.FullName));
            CreateMap<LeadDto, Lead>();

            CreateMap<SaleStage, SaleStageDto>();
            CreateMap<SaleStageDto, SaleStage>();

            CreateMap<BusinessType, BusinessTypeDto>();
            CreateMap<BusinessTypeDto, BusinessType>();

            CreateMap<Opportunity, OpportunityDto>()
                .ForMember(x => x.AssignedToUserId, x => x.MapFrom(c => c.AssignedToUser.UserId))
                .ForMember(c => c.AssignedToUsername, x => x.MapFrom(c => c.AssignedToUser.Username))
                .ForMember(c => c.AssignedToFullName, x => x.MapFrom(c => c.AssignedToUser.FullName))
                .ForMember(x => x.SaleStageDescription, x => x.MapFrom(x => x.SaleStage.Description))
                .ForMember(c => c.AccountName, x => x.MapFrom(c => c.Account.Name))
                .ForMember(x => x.BusinessTypeDescription, x => x.MapFrom(x => x.BusinessType.Description))
                .ForMember(x => x.LeadSourceDescription, x => x.MapFrom(x => x.LeadSource.Description))
                .ForMember(x => x.CurrencyName, x => x.MapFrom(x => x.Currency.Name))
                .ForMember(x => x.CurrencyCode, x => x.MapFrom(x => x.Currency.CurrencyCode))
                .ForMember(x => x.Symbol, x => x.MapFrom(x => x.Currency.Symbol));
            CreateMap<OpportunityDto, Opportunity>();

            CreateMap<LeadToContactDto, Contact>()
                .ForMember(x => x.BillingCity, x => x.MapFrom(c => c.City))
                .ForMember(x => x.BillingState, x => x.MapFrom(c => c.State))
                .ForMember(x => x.BillingPostalCode, x => x.MapFrom(c => c.PostalCode))
                .ForMember(x => x.BillingCountry, x => x.MapFrom(c => c.Country))
                .ForMember(x => x.BillingFullAddress, x => x.MapFrom(c => c.Address))

                .ForMember(x => x.ShippingCity, x => x.MapFrom(c => c.Other_City))
                .ForMember(x => x.ShippingState, x => x.MapFrom(c => c.Other_State))
                .ForMember(x => x.ShippingPostalCode, x => x.MapFrom(c => c.Other_PostalCode))
                .ForMember(x => x.ShippingCountry, x => x.MapFrom(c => c.Other_Country))
                .ForMember(x => x.ShippingFullAddress, x => x.MapFrom(c => c.Other_Address));
        }
    }
}
